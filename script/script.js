const images = document.querySelectorAll(".image-to-show");
const stopButton = document.getElementById("stop");
const startButton = document.getElementById("start");
const intervalDuration = 3000;

function slider() {
    let currentIndex = 0;
    let intervalId;

    function showNextImage() {
        images.forEach(image => image.style.display = "none");

        if (currentIndex + 1 === images.length) {
            currentIndex = 0;
        } else {
            currentIndex = currentIndex + 1;
        }
    
        images[currentIndex].style.display = "block";
    }

    function startSlideshow() {
        showNextImage();
        intervalId = setInterval(showNextImage, intervalDuration);
        startButton.disabled = true;
    }
    
    function stopSlideshow() {
        clearInterval(intervalId);
        startButton.disabled = false; 
    }

    stopButton.addEventListener("click", stopSlideshow);
    startButton.addEventListener("click", startSlideshow);

    startSlideshow();
}

slider();
